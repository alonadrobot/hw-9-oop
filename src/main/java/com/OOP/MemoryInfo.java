package com.OOP;

public class MemoryInfo {
    private int memorySize;
    private double memoryPercent;

    public MemoryInfo(int memorySize, double memoryPercent) {
        this.memorySize = memorySize;
        this.memoryPercent = memoryPercent;
    }

    public int getMemorySize() {
        return memorySize;
    }

    public double getMemoryPercent() {
        return memoryPercent;
    }

    @Override
    public String toString() {
        return "MemoryInfo{" +
                "memorySize=" + memorySize +
                ", memoryPercent=" + memoryPercent +
                '}';
    }
}
