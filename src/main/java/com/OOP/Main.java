package com.OOP;

public class Main {
    public static void main(String[] args) {
        Processor processor1 = new ProcessorX86(5000, 4566, 16, ProcessorX86.ARCHITECTURE);
        Processor processor2 = new ProcessorX86(3200, 1600, 64,ProcessorX86.ARCHITECTURE );
        Processor processor3 = new ProcessorArm(1500, 932, 32,ProcessorArm.ARCHITECTURE );
        Processor processor4 = new ProcessorArm(2450, 25678, 64,ProcessorArm.ARCHITECTURE );
        Processor processor5 = new ProcessorX86(2300, 8, 16, ProcessorX86.ARCHITECTURE);
        Processor processor6 = new ProcessorX86(3500, 16000, 64,ProcessorX86.ARCHITECTURE );
        Processor processor7 = new ProcessorArm(1700, 1634, 32,ProcessorArm.ARCHITECTURE );
        Processor processor8 = new ProcessorArm(2800, 2049, 64,ProcessorArm.ARCHITECTURE );
        Processor processor9 = new ProcessorX86(1260, 1724, 32,ProcessorX86.ARCHITECTURE );
        Processor processor10 = new ProcessorArm(1960, 1548, 16,ProcessorArm.ARCHITECTURE );



        Memory memory1=new Memory(7,new String[]{"wow"});
        Device device1 = new Device(processor1,memory1);
        Memory memory2=new Memory(3,new String[]{"AMX","HI"});
        Device device2 = new Device(processor2,memory2);
        Memory memory3=new Memory(9,new String[]{"RTX","AMX","RYZON"});
        Device device3 = new Device(processor3,memory3);
        Memory memory4=new Memory(8,new String[]{"RYZON"});
        Device device4 = new Device(processor4,memory4);
        Memory memory5=new Memory(5,new String[]{"AMX","INTEL CORE"});
        Device device5 = new Device(processor5,memory5);
        Memory memory6=new Memory(6,new String[]{"RTX","AMX","MAC"});
        Device device6 = new Device(processor6,memory6);
        Memory memory7=new Memory(7,new String[]{"MAC"});
        Device device7 = new Device(processor7,memory7);
        Memory memory8=new Memory(2,new String[]{"AMX","RYZON"});
        Device device8 = new Device(processor8,memory8);
        Memory memory9=new Memory(4,new String[]{"RTX","AMX","INTEL CORE"});
        Device device9 = new Device(processor9,memory9);
        Memory memory10=new Memory(9,new String[]{"RTX","MAC",});
        Device device10 = new Device(processor10,memory10);


        Device[] devices = new Device[10];
        devices[0] = device1;
        devices[1] = device2;
        devices[2] = device3;
        devices[3] = device4;
        devices[4] = device5;
        devices[5] = device6;
        devices[6] = device7;
        devices[7] = device8;
        devices[8] = device9;
        devices[9] = device10;

        for (int i = 0; i < devices.length - 1; i ++) {
            System.out.println(devices[i].toString());
        }
    }
}
