package com.OOP;

import java.util.ArrayList;

public class Filtration {
    public ArrayList<Device> filtration1(Device[] device, String architecture){
        if(device == null){
            return null;
        }
        String str;
        ArrayList<Device> result = new ArrayList<>();
        for(int i = 0; i < device.length; i++){
            if(device[i] == null){
                continue;
            }
            str = device[i].getProcessor().getDetails().split(" ")[2];
            if(str.equals(architecture + ",")){
                result.add(device[i]);
            }
        }
        return result;
    }

    public ArrayList<Device> filtration2(Device[] devices, float frequency,  float cache, int bitCapacity){
        if(devices == null){
            return null;
        }
        ArrayList<Device> result = new ArrayList<>();
        for(int i = 0; i < devices.length; i++){
            if(devices[i] == null){
                continue;
            }
            if(devices[i].getProcessor().getFrequency() >= frequency &&
                    devices[i].getProcessor().getCache() >= cache &&
                    devices[i].getProcessor().getBitCapacity() >= bitCapacity){
                result.add(devices[i]);
            }
        }
        return result;
    }

    public ArrayList<Device> filtration3(Device[] device, int volume){
        if(device == null){
            return null;
        }
        ArrayList<Device> result = new ArrayList<>();
        for(int i = 0; i < device.length; i++){
            if(device[i] == null){
                continue;
            }
            if(device[i].getMemory().getMemoryInfo().getMemorySize() >= volume){
                result.add(device[i]);
            }
        }
        return result;
    }

    public ArrayList<Device> filtration4(Device[] device, int occupiedMemory){
        if(device == null){
            return null;
        }
        ArrayList<Device> result = new ArrayList<>();
        for(int i = 0; i < device.length; i++){
            if(device[i] == null){
                continue;
            }
            if(device[i].getMemory().getMemoryInfo().getMemorySize() >= occupiedMemory){
                result.add(device[i]);
            }
        }
        return result;
    }
}
