package com.OOP;

import java.util.Objects;

public abstract class Processor {
    private float frequency;
    private float cache;
    private int bitCapacity;

    public Processor(float frequency, float cache, int bitCapacity) {
        this.frequency = frequency;
        this.cache = cache;
        this.bitCapacity = bitCapacity;
    }

    public String getDetails(){
        return Double.toString(getFrequency())+" "+Double.toString(getCache())+" "+Double.toString(getBitCapacity());
    }

    public double getFrequency() { return frequency; }

    public double getCache() { return cache; }

    public double getBitCapacity() { return bitCapacity; }

    @Override
    public String toString() {
        return "Processor{" +
                "frequency=" + frequency +
                ", cache=" + cache +
                ", bitCapacity=" + bitCapacity +
                '}';
    }
    abstract String dataProcess(String data);
    abstract String dataProcess(long data);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Processor processor = (Processor) o;
        return Float.compare(processor.frequency, frequency) == 0 && Float.compare(processor.cache, cache) == 0 && bitCapacity == processor.bitCapacity;
    }

    @Override
    public int hashCode() {
        return Objects.hash(frequency, cache, bitCapacity);
    }



}


