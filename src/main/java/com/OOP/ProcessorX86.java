package com.OOP;

public class ProcessorX86 extends Processor{
    public static final String ARCHITECTURE="X86";

    public ProcessorX86(float frequency, float cache, int bitCapacity,String ARCHITECTURE) {
        super(frequency, cache, bitCapacity);
    }

    @Override
    String dataProcess(String data) {
        return data.toLowerCase();
    }

    @Override
    String dataProcess(long data) {
        return Long.toString(data).toLowerCase();
    }

    @Override
    public String toString() {
        return "ProcessorX86{" +
                "ARCHITECTURE='" + ARCHITECTURE + '\'' +
                '}'+super.toString();
    }
}
