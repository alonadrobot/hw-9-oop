package com.OOP;

import java.util.ArrayList;
import java.util.List;

public class Device {
    private Processor processor;
    private Memory memory;

    @Override
    public String toString() {
        return "Device{" +
                "processor=" + processor +
                ", memory=" + memory +
                '}';
    }

    public Device(Processor processor, Memory memory) {
        this.processor = processor;
        this.memory = memory;
    }

    public Processor getProcessor() {
        return processor;
    }

    public Memory getMemory() {
        return memory;
    }
    public void save(String[]data){
        for(String str:data){
            memory.save(str);
        }
    }
    public String[] readAll(){
        List<String> list=new ArrayList<>();
        list.add(memory.readLast());
        memory.removeLast();
        String[] result = new String[list.size()];
        result=list.toArray(result);
        return result;
    }
    public void dataProcessing(){

    }
    public String getSystemInfo(){
        return processor.getDetails() + memory.getMemoryInfo().toString();
    }
}
