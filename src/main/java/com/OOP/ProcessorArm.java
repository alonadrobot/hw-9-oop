package com.OOP;

import java.util.Locale;

public class ProcessorArm extends Processor{
    public static final String ARCHITECTURE="ARM";

    public ProcessorArm(float frequency, float cache, int bitCapacity,String ARCHITECTURE) {
        super(frequency, cache, bitCapacity);
    }

    @Override
    String dataProcess(String data) {
        return data.toUpperCase();
    }

    @Override
    String dataProcess(long data) {
        return Long.toString(data).toUpperCase();
    }

    @Override
    public String toString() {
        return "ProcessorArm{" +
                "ARCHITECTURE='" + ARCHITECTURE + '\'' +
                '}'+super.toString();
    }
}
